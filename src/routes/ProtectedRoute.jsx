import { Route, Redirect } from 'react-router-dom';
import { useUserAuth } from 'src/hooks/authentication';

function ProtectedRoute({ component: Component, ...restProps }) {
  const userAuth = useUserAuth();

  return (
    <Route
      {...restProps}
      render={(props) =>
        !userAuth.isAuth ? (
          <Redirect
            to={{
              pathname: '/',
              state: {
                from: props.location,
              },
            }}
          />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
}

export default ProtectedRoute;
