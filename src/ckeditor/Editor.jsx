import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import { editorConfiguration } from './config';

export default function Editor({ data, onChange }) {
  return data ? (
    <CKEditor
      editor={ClassicEditor}
      data={data}
      config={editorConfiguration}
      onChange={(_, editor) => {
        const data = editor.getData();
        onChange(data);
      }}
    />
  ) : (
    <CKEditor
      editor={ClassicEditor}
      config={editorConfiguration}
      onChange={(_, editor) => {
        const data = editor.getData();
        onChange(data);
      }}
    />
  );
}
