import Cookies from 'js-cookie';

export function getCookieTokenConfirmAccount() {
  return Cookies.get('__token_confirm_account') || null;
}

export function getTokenAuth() {
  return Cookies.get('__token');
}

export function setToken(token = '') {
  Cookies.set('__token', token, {
    sameSite: 'Lax',
  });
}

export const PAGES = parseInt(Cookies.get('__pages')) || 0;
