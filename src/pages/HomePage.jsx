// Components
import { Button, Card, Col, Container, Jumbotron, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function HomePage() {
  return (
    <Row>
      <Col sm={12} md={12} xl={12}>
        <Jumbotron fluid className='bg-dark text-white'>
          <Container className='text-center'>
            <h1>Ver todas las Publicaciones</h1>
            <p>Publicaciones referidas a noticias, investigaciones....</p>
            <br />
            <Link className='btn btn-light text-primary font-weight-bold' to='/posts'>
              Ver las publicaciones
            </Link>
          </Container>
        </Jumbotron>
      </Col>
      <Col sm={6} md={6} xl={6}>
        <Card>
          <Card.Header>
            <Card.Title>Card 1</Card.Title>
          </Card.Header>
          <Card.Body>
            <Card.Title>Card Title</Card.Title>
            <Card.Text>
              Some quick example text to build on the card title and make up the bulk of the card's
              content.
            </Card.Text>
            <Button variant='primary'>Go somewhere</Button>
          </Card.Body>
        </Card>
      </Col>
      <Col sm={6} md={6} xl={6}>
        <Card>
          <Card.Header>
            <Card.Title>Card 1</Card.Title>
          </Card.Header>
          <Card.Body>
            <Card.Title>Card Title</Card.Title>
            <Card.Text>
              Some quick example text to build on the card title and make up the bulk of the card's
              content.
            </Card.Text>
            <Button variant='primary'>Go somewhere</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

export default HomePage;
