import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import { Alert, Button, Col, Form, Row, Spinner } from 'react-bootstrap';
import { getUserAction, userRegisterAction } from 'src/app/actions/userActions';
import { useUserAuth } from 'src/hooks/authentication';
import { resetAction } from 'src/app/actions';

const defaultValues = {
  email: '',
  password: '',
  passwordRepeat: '',
  username: '',
};

function RegisterPage() {
  const userAuth = useUserAuth();
  const history = useHistory();
  const dispatch = useDispatch();
  const form = useForm({ defaultValues });
  const touched = form.formState.touched;

  const handleRegisterSubmit = (data) => {
    const fd = new FormData();
    fd.append('username', data.username);
    fd.append('email', data.email);
    fd.append('password', data.password);
    fd.append('password_repeat', data.passwordRepeat);

    dispatch(userRegisterAction(fd));
    dispatch(getUserAction());
  };

  React.useEffect(() => {
    userAuth.isAuth && history.goBack();

    return () => {};
  }, [userAuth.isAuth, history]);

  return (
    <Row className='justify-content-center'>
      <Col md={6} xl={6} lg={6}>
        <h2 className='text-center'>
          Registrarse en <span className='text-primary'>Blog</span>
        </h2>
        {userAuth.loading && <Spinner animation='grow' variant='primary' />}
        <Alert
          show={!!userAuth.error}
          variant='danger'
          onClose={() => dispatch(resetAction())}
          dismissible
        >
          {!!userAuth.error &&
            userAuth?.error.map((err) => (
              <span className='d-block' key={err.field}>
                {err.message[0]}
              </span>
            ))}
        </Alert>

        <Form onSubmit={form.handleSubmit(handleRegisterSubmit)}>
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control
              ref={form.register({ required: true })}
              name='email'
              type='email'
              placeholder='example@mail.org'
            />
            {form.errors.email && (
              <Form.Text className='text-danger'>El correo es necesario.</Form.Text>
            )}
          </Form.Group>

          <Form.Group>
            <Form.Label>Nombre de Usuario</Form.Label>
            <Form.Control
              name='username'
              type='text'
              placeholder='username'
              ref={form.register({
                required: true,
                minLength: {
                  value: 4,
                  message: 'El nombre de usuario necesita debe ser mayor a 4 caracteres.',
                },
                maxLength: {
                  value: 20,
                  message: 'El nombre de usuario necesita no debe ser mayor a 20 caracteres.',
                },
              })}
            />
            {form.errors.username && (
              <Form.Text className='text-danger'>El nombre de usuario es necesario.</Form.Text>
            )}
            {form.errors.username?.message && (
              <Form.Text className='text-danger'>{form.errors.username.message}</Form.Text>
            )}
          </Form.Group>
          <Form.Group>
            <Form.Label>Contraseña</Form.Label>
            <Form.Control
              name='password'
              type='password'
              placeholder='Contraseña'
              autoComplete='off'
              ref={form.register({
                required: true,
                minLength: {
                  value: 4,
                  message: 'La contraseña debe de ser al menos de 4 caracteres',
                },
                maxLength: {
                  value: 20,
                  message: 'El contraseña necesita no debe ser mayor a 20 caracteres.',
                },
              })}
            />
            {form.errors.password && (
              <Form.Text className='text-danger'>Las contraseña de usuario es necesario.</Form.Text>
            )}
            {form.errors.password?.message && (
              <Form.Text className='text-danger'>{form.errors.password.message}</Form.Text>
            )}
          </Form.Group>
          <Form.Group>
            <Form.Label>Confirmar la contraseña</Form.Label>
            <Form.Control
              name='passwordRepeat'
              type='password'
              placeholder='Confirmar la contraseña'
              autoComplete='off'
              ref={form.register({ required: true })}
            />
            <Form.Text className='text-muted'>
              La contraseña debe de tener al menos un carácter
            </Form.Text>
            {form.watch('password') !== form.watch('passwordRepeat') && touched.passwordRepeat && (
              <Form.Text className='text-danger'>Las contraseñas no coinciden.</Form.Text>
            )}
          </Form.Group>
          <Form.Group>
            <Button
              disabled={form.watch('password') !== form.watch('passwordRepeat')}
              type='submit'
              className='btn btn-block btn-primary'
            >
              Registrarse
            </Button>
          </Form.Group>
        </Form>
      </Col>
    </Row>
  );
}

export default RegisterPage;
