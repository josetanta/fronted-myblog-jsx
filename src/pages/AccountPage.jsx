import React from 'react';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Alert, Col, Row } from 'react-bootstrap';
import UserForm from 'src/components/forms/UserForm';
import UserCard from 'src/components/UserCard';
import { useUserAuth } from 'src/hooks/authentication';
import { resetAction } from 'src/app/actions';

function AccountPage() {
  const userAuth = useUserAuth();
  const dispatch = useDispatch();
  const methodsForm = useForm({
    defaultValues: { ...userAuth.user, image: '', activeEdit: true },
  });
  const onReset = () => {
    methodsForm.reset({ ...userAuth.user, image: '', activeEdit: true });
  };

  let newMmethodsForm = Object.assign({}, methodsForm, { onReset });

  return (
    <React.Fragment>
      <Row className='gap-2'>
        <Col md={6} xl={6} lg={6}>
          <UserCard user={userAuth.user} methodsForm={newMmethodsForm} />
        </Col>
        <Col md={6} xl={6} lg={6}>
          <Alert
            show={!!userAuth.message}
            onClose={() => dispatch(resetAction())}
            variant='success'
            dismissible
          >
            <span dangerouslySetInnerHTML={{ __html: userAuth.message }} />
          </Alert>
          <UserForm
            user={userAuth.user}
            isConfirmed={userAuth.user.confirmed}
            methodsForm={newMmethodsForm}
          />
        </Col>
      </Row>
    </React.Fragment>
  );
}

export default AccountPage;
