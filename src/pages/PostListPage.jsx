import { Fragment, useState } from 'react';

import { usePostList } from 'src/hooks/posts';

// Components
import { Row, Spinner } from 'react-bootstrap';
import Pagination from 'src/components/Pagination';
import PostList from 'src/components/PostList';

function PostListPage() {
  const [page, setPage] = useState(0);
  const postList = usePostList(page);

  return (
    <Fragment>
      <Row>
        <Pagination setPage={setPage} />
      </Row>
      <Row>
        {postList.isLoading ? (
          <Spinner animation='border' variant='success' />
        ) : (
          <PostList posts={postList.data} />
        )}
      </Row>
    </Fragment>
  );
}

export default PostListPage;
