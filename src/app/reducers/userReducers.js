import * as typeAction from 'src/app/type-actions';

let initState = {
  isAuth: false,
  isReady: false,
  user: null,
  message: '',
  loading: false,
  error: undefined,
  success: false,
};

export function userLoginReducer(state = initState, action = { payload: {}, type: '' }) {
  switch (action.type) {
    case typeAction.USER_LOGIN_BEGIN:
      return Object.assign({}, state, { loading: true });

    case typeAction.USER_LOGIN_SUCCESS: {
      let token = action.payload.token;

      state.message = action.payload.message;
      return Object.assign({}, state, {
        token,
        isReady: true,
        loading: false,
        success: true,
      });
    }
    case typeAction.USER_LOGIN_FAIL: {
      let error = action.payload.errors;
      let message = action.payload.message;
      let isReady = true;
      let loading = false;

      return Object.assign({}, state, { error, isReady, loading, message });
    }

    case typeAction.USER_ACCOUNT_SUCCESS: {
      let user = action.payload;
      let isAuth = Boolean(user);
      let isReady = Boolean(user);
      return Object.assign({}, state, {
        user,
        isReady,
        isAuth,
        success: true,
      });
    }

    case typeAction.USER_ACCOUNT_ERROR:
      return Object.assign({}, state, {
        user: null,
        isAuth: false,
        isReady: true,
      });

    case typeAction.USER_UPDATE_SUCCESS: {
      let user = action.payload.user;
      let message = action.payload.message;
      return Object.assign({}, state, {
        success: true,
        loading: false,
        user,
        message,
      });
    }

    case typeAction.USER_UPDATE_FAIL: {
      return Object.assign({}, state, { message: action.payload });
    }

    case typeAction.USER_LOGOUT_BEGIN:
      return Object.assign({}, state, { loading: true });

    case typeAction.USER_LOGOUT_SUCCESS:
      return Object.assign({}, initState, {
        isAuth: false,
        user: null,
        isReady: true,
        success: true,
        loading: false,
      });

    case typeAction.USER_REGISTER_BEGIN:
      return Object.assign({}, state, { loading: true });

    case typeAction.USER_REGISTER_SUCCESS: {
      let user = action.payload.data;
      let token = action.payload.token;
      let message = action.payload.message;
      let isAuth = Boolean(user);
      let isReady = Boolean(user);

      return Object.assign({}, state, {
        token,
        user,
        message,
        isAuth,
        isReady,
        loading: false,
        success: true,
      });
    }

    case typeAction.USER_REGISTER_FAIL:
      return Object.assign({}, state, { error: action.payload, loading: false });

    case typeAction.RESET_ERROR_SUCCESS_MESSAGE: {
      let error = undefined;
      let message = '';
      let success = false;
      return Object.assign({}, state, { error, message, success });
    }

    case typeAction.AUTH_CONFIRM_BEGIN:
      return Object.assign({}, state, { loading: true });

    case typeAction.AUTH_CONFIRM_SUCCESS:
      return Object.assign({}, state, {
        message: action.payload,
        loading: false,
        success: true,
        isAuth: Boolean(state.user),
      });

    case typeAction.AUTH_CONFIRM_FAIL:
      return Object.assign({}, state, {
        error: action.payload,
        loading: false,
      });

    default:
      return state;
  }
}
