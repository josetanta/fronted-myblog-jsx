export const USER_LOGIN_BEGIN = '[UserLogin] USER_LOGIN_BEGIN';
export const USER_LOGIN_SUCCESS = '[UserLogin] USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAIL = '[UserLogin] USER_LOGIN_FAIL';

export const USER_ACCOUNT_SUCCESS = '[User] USER_ACCOUNT_SUCCESS';
export const USER_ACCOUNT_ERROR = '[User] USER_ACCOUNT_ERROR';

export const USER_LOGOUT_BEGIN = '[UserLogin] USER_LOGOUT_BEGIN';
export const USER_LOGOUT_SUCCESS = '[UserLogin] USER_LOGOUT_SUCCESS';

export const USER_REGISTER_BEGIN = '[UserRegister] USER_REGISTER_BEGIN';
export const USER_REGISTER_SUCCESS = '[UserRegister] USER_REGISTER_SUCCESS';
export const USER_REGISTER_FAIL = '[UserRegister] USER_REGISTER_FAIL';

export const USER_UPDATE_BEGIN = '[UserUpdate] USER_UPDATE_BEGIN';
export const USER_UPDATE_SUCCESS = '[UserUpdate] USER_UPDATE_SUCCESS';
export const USER_UPDATE_FAIL = '[UserUpdate] USER_UPDATE_SUCCESS';

export const AUTH_CONFIRM_BEGIN = '[AuthConfirm] AUTH_CONFIRM_BEGIN';
export const AUTH_CONFIRM_SUCCESS = '[AuthConfirm] AUTH_CONFIRM_SUCCESS';
export const AUTH_CONFIRM_FAIL = '[AuthConfirm] AUTH_CONFIRM_FAIL';

export const RESET_ERROR_SUCCESS_MESSAGE = '[State] RESET_ERROR_SUCCESS_MESSAGE';
