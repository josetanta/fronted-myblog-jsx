import React from 'react';
import ReactDOM from 'react-dom';
import BlogApp from 'src/BlogApp';
import 'src/stylescss/index.css';

const rootDOM = document.getElementById('blog-app');

const AppRoot = (
  <React.StrictMode>
    <BlogApp />
  </React.StrictMode>
);

if (rootDOM.hasChildNodes()) ReactDOM.hydrate(AppRoot, rootDOM);
else ReactDOM.render(AppRoot, rootDOM);
