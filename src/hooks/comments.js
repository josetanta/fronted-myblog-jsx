import { useMutation, useQuery, useQueryClient } from 'react-query';
import { deleteComment, getCommentList, saveComment, updateComment } from 'src/api/comment';

export function useCommentSaveMutation({ postId = '', uuidPost = '' }) {
  const queryClient = useQueryClient();
  return useMutation((newComment) => saveComment(newComment, postId), {
    onMutate: async (newComment) => {
      await queryClient.cancelQueries(['comment-list', uuidPost]);

      const previousComments = queryClient.getQueryData(['comment-list', uuidPost]);

      queryClient.setQueryData(['comment-list', uuidPost], (prev) => ({
        ...prev,
        newComment,
      }));
      return previousComments;
    },
    onError: (_err, _newComment, context) => {
      queryClient.setQueryData(['comment-list', uuidPost], context);
    },
    onSuccess: () => {
      queryClient.setQueryData(['errors', uuidPost], undefined);
    },
    onSettled: async (newData) => {
      queryClient.setQueryData(['errors', uuidPost], () => newData.errors);
      await queryClient.invalidateQueries(['comment-list', uuidPost]);
    },
  });
}

export function useCommentUpdateMutation({ commentId = '', postId = '', uuidPost = '' }) {
  const queryClient = useQueryClient();

  return useMutation((commentUpdate) => updateComment(commentUpdate, commentId, postId), {
    mutationKey: ['comment-list', uuidPost],
    onMutate: async (updateComment) => {
      await queryClient.cancelQueries(['comment-list', uuidPost]);

      const previousComments = queryClient.getQueryData(['comment-list', uuidPost]);

      queryClient.setQueryData(['comment-list', uuidPost], (prev) => {
        return {
          ...prev,
          updateComment,
        };
      });

      return previousComments;
    },
    onError: (err, variables, context) => {
      queryClient.setQueryData(['comment-list', uuidPost], context);
    },
    onSettled: async () => {
      await queryClient.invalidateQueries(['comment-list', uuidPost]);
    },
  });
}

export function useCommentRemoveMutation({ postId = '', uuidPost = '' }) {
  const queryClient = useQueryClient();
  return useMutation((commentId) => deleteComment(commentId, postId), {
    mutationKey: ['comment-list', uuidPost],
    onMutate: async () => {
      await queryClient.cancelQueries(['comment-list', uuidPost]);

      const previousComments = queryClient.getQueryData(['comment-list', uuidPost]);

      queryClient.setQueryData(['comment-list', uuidPost], (prev) => prev);

      return previousComments;
    },
    onError: (_err, _updateComment, context) => {
      queryClient.setQueryData(['comment-list', uuidPost], context);
    },
    onSettled: async () => {
      await queryClient.invalidateQueries(['comment-list', uuidPost]);
    },
  });
}

/**
 *
 * @param {string} uuidPost
 * @returns
 */
export function useCommentsListByPost(uuidPost) {
  return useQuery(['comment-list', uuidPost], () => getCommentList(uuidPost), {
    select: (comments) => comments.data,
  });
}
