import { useState } from 'react';

/**
 *
 * @param {any} initState
 * @returns {{
 *  handleDataChange: () => void,
 *  ...formData: initState
 * }}
 */
export function useFormData(initState = {}) {
  const [formData, setFormData] = useState(initState);

  const handleDataChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return {
    handleDataChange,
    ...formData,
  };
}
