import { useMutation, useQuery, useQueryClient } from 'react-query';
import { deletePost, getPost, getPostList, savePost, updatePost } from 'src/api/post';

/**
 *
 * @param {number} page
 * @returns
 */
export function usePostList(page) {
  const queryClient = useQueryClient();
  return useQuery(['posts', page], () => getPostList(page), {
    onSettled: () => {
      queryClient.getQueryData(['posts', page]);
    },
    select: (posts) => posts.data,
  });
}

export function usePostUpdateMutation({ uuidPost = '', postId = '' }) {
  const queryClient = useQueryClient();

  return useMutation((postForm) => updatePost(postId, postForm), {
    mutationKey: ['post-details', uuidPost],
    onMutate: async (updatePost) => {
      await queryClient.cancelQueries(['post-details', uuidPost]);

      const previousPost = queryClient.getQueryData(['post-details', uuidPost]);

      queryClient.setQueryData(['post-details', uuidPost], (prev) => ({
        ...prev,
        updatePost,
      }));

      return previousPost;
    },
    onError: (err, updatePost, context) => {
      queryClient.setQueryData(['post-details', uuidPost], { ...context });
    },
    onSettled: async () => {
      await queryClient.invalidateQueries(['post-details', uuidPost]);
    },
  });
}

export function usePostSaveMutation() {
  const queryClient = useQueryClient();

  return useMutation(savePost, {
    onMutate: async (newPost) => {
      await queryClient.cancelQueries('posts');

      const previousPosts = queryClient.getQueryData('posts');

      queryClient.setQueryData('posts', (prev) => ({ ...prev, newPost }));

      return previousPosts;
    },
    onError: (err, newPost, context) => {
      queryClient.setQueryData('posts', { ...context });
    },
    onSettled: async () => {
      await queryClient.invalidateQueries('posts');
    },
  });
}

export function usePostRemoveMutation() {
  const queryClient = useQueryClient();
  return useMutation(deletePost, {
    onMutate: async () => {
      await queryClient.cancelQueries('posts');

      const previousPosts = queryClient.getQueryData('posts');

      queryClient.setQueryData('posts', (prev) => ({ ...prev }));

      return previousPosts;
    },
    onError: (err, vars, context) => {
      queryClient.setQueryData('posts', { ...context });
    },
    onSettled: async () => {
      await queryClient.invalidateQueries('posts');
    },
  });
}

/** @param {string} uuidPost */
export function usePostDetail(uuidPost) {
  return useQuery(['post-details', uuidPost], () => getPost(uuidPost), {
    select: (post) => post.data,
  });
}
