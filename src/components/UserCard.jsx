import React from 'react';
import { Button, Card } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { removeImageAccountAction } from 'src/app/actions';

function UserCard({ user, methodsForm }) {
  const watch = methodsForm.watch;
  const dispatch = useDispatch();

  const handleRemoveImageClick = () => {
    dispatch(removeImageAccountAction());
  };

  return (
    <Card>
      <section className='relative'>
        {!String(user.image).includes('avatar.svg') && (
          <Button
            onClick={handleRemoveImageClick}
            title='Eliminar imagen'
            className='absolute btn__fixed-trash'
            variant='danger'
          >
            <i className='fal fa-trash-alt' />
          </Button>
        )}
        <Card.Img className='img__cover' variant='top' src={user.image} />
      </section>
      <Card.Body>
        <Card.Header>
          <Card.Title className='text-dark'>
            Nombre de usuario:
            <h2 className='text-break'>{watch('username')}</h2>
          </Card.Title>
          Nombre:
          <h4 className='text-break'>{watch('name')}</h4>
          <div>
            Correo: <br />
            <span className='text-break'>{watch('email')}</span>
          </div>
        </Card.Header>
      </Card.Body>
    </Card>
  );
}
export default React.memo(UserCard);
