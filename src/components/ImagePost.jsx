export default function ImagePost({ image, className, alt = '' }) {
  // + "post-img"
  return <img className={className} src={image} alt={alt} />;
}
