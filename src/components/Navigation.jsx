import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { userLogoutAction } from 'src/app/actions/userActions';

// Components
import { Button, Container, Dropdown, Nav, Navbar } from 'react-bootstrap';
import ModalPost from './modals/ModalPost';
import ModalLogin from './modals/ModalLogin';
import { useUserAuth } from 'src/hooks/authentication';
import { AuthActionPerm, AnonymousPerm, AuthPermWrite } from 'src/components/utils/AuthPerm';

function Navigation() {
  const [modalLogin, setModalLogin] = React.useState(false);
  const [modalShowPost, setModalShowPost] = React.useState(false);
  const userAuth = useUserAuth();
  const dispatch = useDispatch();

  const userAnonymousNode = (
    <React.Fragment>
      <Link to='/register' className='nav-link'>
        <i className='far fa-user-plus'> </i> Registrarse
      </Link>
      <Button className='nav-link border-0 btn-primary' onClick={() => setModalLogin(true)}>
        <i className='fas fa-sign-in-alt' /> Iniciar Sesión
      </Button>
    </React.Fragment>
  );
  return (
    <React.Fragment>
      <Navbar bg='primary' expand='lg' variant='dark' sticky='top' className='mb-3'>
        <Container>
          <Link to='/' className='navbar-brand'>
            Blog
          </Link>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav>
              <Link to='/inicio' className='nav-link'>
                <i className='fas fa-home-lg-alt' />
              </Link>

              <AuthPermWrite>
                <Button
                  variant='outline-primary'
                  className='nav-link'
                  onClick={() => setModalShowPost(true)}
                >
                  <i className='far fa-plus-circle' /> Crear una publicación
                </Button>
              </AuthPermWrite>

              <Link to='/posts' className='nav-link'>
                Publicaciones
              </Link>

              <Link to='/about' className='nav-link'>
                Sobre nosotros
              </Link>

              <Link to='/' target='_blank' className='nav-link'>
                <i className='fab fa-github' /> GitHub
              </Link>
            </Nav>
            <div className='flex-grow-1' />
            <Nav>
              {userAuth.isAuth && (
                <Dropdown drop='left'>
                  <Dropdown.Toggle variant='primary' id='dropdown-basic'>
                    <img
                      className='account__img__profile'
                      src={userAuth.user?.image}
                      alt={userAuth.user?.name}
                    />
                  </Dropdown.Toggle>

                  <Dropdown.Menu align='left'>
                    <Link
                      className='dropdown-item'
                      role='button'
                      to={'/user/' + userAuth.user?.uuid}
                    >
                      <i className='fas fa-user-circle' /> Mi perfil
                    </Link>
                    <Link className='dropdown-item' role='button' to='/profile'>
                      <i className='fas fa-user-edit' /> Editar mi perfil
                    </Link>
                    <Button
                      variant={'danger'}
                      className='dropdown-item'
                      onClick={() => dispatch(userLogoutAction())}
                    >
                      <i className='fas fa-sign-out-alt' /> Cerrar Sesión
                    </Button>
                  </Dropdown.Menu>
                </Dropdown>
              )}
              <AnonymousPerm>{userAnonymousNode}</AnonymousPerm>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <AuthActionPerm>
        <ModalPost modalShowPost={modalShowPost} setModalShowPost={setModalShowPost} />
      </AuthActionPerm>
      <AnonymousPerm>
        <ModalLogin modalLogin={modalLogin} setModalLogin={setModalLogin} />
      </AnonymousPerm>
    </React.Fragment>
  );
}

export default Navigation;
