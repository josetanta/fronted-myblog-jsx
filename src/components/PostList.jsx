import { Col } from 'react-bootstrap';
import withErrorBoundary from 'src/hocs/withErrorBoundary';
import Post from './Post';

function PostList({ posts, col = 11 }) {
  return (
    <Col className='d-flex flex-column gap-3' sm={col} md={col} xl={col}>
      {posts.map((post, index) => (
        <Post
          key={post.attributes.title + (index + 1)}
          post={post}
          linkMoreInfoStatus
          truncateText
        />
      ))}
    </Col>
  );
}
export default withErrorBoundary(PostList);
