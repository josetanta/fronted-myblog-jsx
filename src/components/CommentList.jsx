import React from 'react';
import Comment from './Comment';

function CommentList({ isLoading = false, isError = false, comments = [] }) {
  return (
    <div className='comment__list my-4 media-body'>
      {isLoading ? (
        <div>Cargando los comentarios</div>
      ) : isError ? (
        <div>Error al cargar los comentarios</div>
      ) : (
        <React.Fragment>
          {comments?.map((comment, index) => (
            <Comment comment={comment} key={index} />
          ))}
        </React.Fragment>
      )}
    </div>
  );
}

export default CommentList;
