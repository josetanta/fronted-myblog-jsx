import React from 'react';
import { useForm } from 'react-hook-form';

import { Button, Form } from 'react-bootstrap';
import { useCommentSaveMutation, useCommentUpdateMutation } from 'src/hooks';
import Editor from 'src/ckeditor/Editor';

function CommentForm({ postId, uuidPost, commentId, isEdit, content, setIsComment, setIsEdit }) {
  const commentSave = useCommentSaveMutation({ postId, uuidPost });
  const commentUpdate = useCommentUpdateMutation({ postId, uuidPost, commentId });
  const hookForm = useForm();

  const [contentText, setContentText] = React.useState(() => {
    if (isEdit) {
      return content;
    } else {
      return '';
    }
  });

  const handleSubmit = async (data) => {
    data = { post_id: postId, content: contentText };
    if (isEdit) {
      commentUpdate.mutateAsync(data);
      setIsEdit(false);
    } else {
      commentSave.mutateAsync(data);
      setIsComment(false);
    }
  };

  return (
    <React.Fragment>
      <Form
        className='d-flex flex-column justify-content-between'
        onSubmit={hookForm.handleSubmit(handleSubmit)}
      >
        <div className='w-full'>
          <Form.Group className='d-flex justify-content-between align-items-center'>
            <Button type='submit' variant={isEdit ? 'primary' : 'success'}>
              {isEdit ? (
                <React.Fragment>
                  <i className='fad fa-comment-edit' /> Guardar Cambios
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <i className='fas fa-save' /> Guardar comentario
                </React.Fragment>
              )}
            </Button>
            {!isEdit && (
              <Button
                variant='dark'
                children={<i className='fas fa-times' />}
                onClick={() => setIsComment(false)}
              />
            )}
          </Form.Group>

          <Form.Group>
            {isEdit ? (
              <Editor data={content} onChange={setContentText} />
            ) : (
              <Editor onChange={setContentText} />
            )}
          </Form.Group>
        </div>
      </Form>
    </React.Fragment>
  );
}

export default CommentForm;
