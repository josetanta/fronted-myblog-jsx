import React from 'react';
import { useForm as useFormHookReact } from 'react-hook-form';

import { Button, Form, Spinner } from 'react-bootstrap';
import { useFormData } from 'src/hooks/useForm';
import { usePostSaveMutation, usePostUpdateMutation } from 'src/hooks/posts';
import Editor from 'src/ckeditor/Editor';

function PostForm({ post, editPost, setEditPost, setModalShowPost, modalShowPost }) {
  const mutationSave = usePostSaveMutation();
  const mutationUpdate = usePostUpdateMutation({
    uuidPost: post?.uuid,
    postId: post?.id,
  });

  const postUpdate = post?.attributes;

  const [contentSave, setContentSave] = React.useState(() => {
    return editPost ? postUpdate.content : '';
  });

  const { register, handleSubmit, errors } = useFormHookReact();

  const { handleDataChange, ...formData } = useFormData(postUpdate);

  const handleSubmitForm = async (data, e) => {
    e.preventDefault();
    const fd = new FormData();
    fd.append('image', data.image[0]);
    fd.append('content', contentSave);
    fd.append('title', data.title);
    if (editPost) {
      await mutationUpdate.mutateAsync(fd);
      setEditPost(!editPost);
    } else {
      await mutationSave.mutateAsync(fd);
      setModalShowPost(!modalShowPost);
    }
  };

  return (
    <Form onSubmit={handleSubmit(handleSubmitForm)}>
      {mutationSave.isLoading && <Spinner animation='border' variant='primary' />}
      {(postUpdate && (
        <React.Fragment>
          <Form.Group>
            <img className='post-img' src={formData?.image} alt={formData?.title} />
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor='image'>Imagen de la Publicación</Form.Label>
            <Form.File
              type='file'
              name='image'
              className='form-control'
              accept='image/*'
              ref={register({
                required: false,
              })}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor='title'>
              <b>Titulo de la publicación</b>
            </Form.Label>
            <Form.Control
              isInvalid={!!errors.title}
              type='text'
              value={formData?.title || ''}
              onChange={handleDataChange}
              name='title'
              ref={register({ required: true })}
            />
            {errors.title && (
              <div className='small text-danger'>Por favor ponga el titulo de la Publicación</div>
            )}
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor='content'>
              <b>Contenido de la publicación</b>
            </Form.Label>
            <Editor data={formData?.content || ''} onChange={setContentSave} />
          </Form.Group>

          <Form.Group>
            <Button type='submit' variant='success' block>
              Guardar los cambios
            </Button>
          </Form.Group>
        </React.Fragment>
      )) || (
        <React.Fragment>
          <Form.Group>
            <Form.Label htmlFor='title'>Titulo de la publicación</Form.Label>
            <Form.Control
              isInvalid={!!errors.title}
              type='text'
              name='title'
              ref={register({ required: true })}
            />
            {errors.title && (
              <div className='small text-danger'>Por favor ponga el titulo de la Publicación</div>
            )}
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor='content'>Contenido de la publicación</Form.Label>
            <Editor onChange={setContentSave} />
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor='image'>Imagen de la Publicación</Form.Label>
            <Form.File
              type='file'
              name='image'
              className='form-control border-0'
              accept='image/*'
              ref={register({
                required: false,
              })}
            />
          </Form.Group>
          <Form.Group>
            <Button type='submit' variant='outline-success' block>
              Publicar
            </Button>
          </Form.Group>
        </React.Fragment>
      )}
    </Form>
  );
}

export default PostForm;
