import { Fragment } from 'react';
import ImagePost from './ImagePost';

export default function PostHeader({ post }) {
  return (
    <Fragment>
      <h1 className='post__head__title'>{post?.attributes.title}</h1>
      <ImagePost className='post__header__img' image={post?.attributes.image} />
    </Fragment>
  );
}
