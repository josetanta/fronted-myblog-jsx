import { Link, useHistory } from 'react-router-dom';

// Components
import { Button } from 'react-bootstrap';
import ImagePost from './ImagePost';
import ImageUser from './ImageUser';
import { AuthPerm } from 'src/components/utils/AuthPerm';
import { usePostRemoveMutation } from 'src/hooks/posts';
import { formatDatetime } from 'src/utils/moment';

function Post({ post, linkMoreInfoStatus, truncateText, showActions, onEditPost, editPost }) {
  const mutationRemove = usePostRemoveMutation();
  const history = useHistory();

  let user = post.relationships?.users?.data;
  const handleRemovePostClick = () => {
    if (window.confirm('Estas seguro de eliminar tu publicación')) {
      mutationRemove.mutate(post.id);
      return history.push('/posts');
    }
  };

  const ShowAction = (
    <AuthPerm user={user}>
      <div className='mt-2 mb-2'>
        <Button
          variant='light'
          onClick={() => {
            onEditPost(!editPost);
          }}
        >
          <i className='fal fa-pencil' />
        </Button>
        <span className='mr-2' />
        <Button onClick={handleRemovePostClick} variant='danger'>
          <i className='fal fa-trash-alt' />
        </Button>
      </div>
    </AuthPerm>
  );

  return (
    <div className='my-2 rounded shadow py-5'>
      <div className='post-content'>
        {truncateText && (
          <div className='text-center'>
            <Link className='text-primary text-decoration-none' to={'/posts/' + post.uuid}>
              <h1>{post.attributes.title}</h1>
              <ImagePost className='post-img' alt={post.slug} image={post.attributes.image} />
            </Link>
          </div>
        )}
        {showActions && ShowAction}
        {(truncateText && (
          <div
            className='my-2 px-2 text-md-justify text-break text-content-size'
            dangerouslySetInnerHTML={{
              __html: `${post.attributes.content.slice(
                0,
                post.attributes.content.length / 4
              )}  ...`,
            }}
          />
        )) || (
          <div
            dangerouslySetInnerHTML={{ __html: post.attributes.content }}
            className='my-2 px-4 text-md-justify text-break text-content-size'
          />
        )}

        {linkMoreInfoStatus && (
          <Link className='text-primary' to={'/posts/' + post.uuid}>
            Más información aquí <i className='fad fa-external-link' />
          </Link>
        )}

        <p className='text-muted'>
          <b>Publicado:</b> {formatDatetime(post.attributes.created)}
        </p>
      </div>

      <div className='meta-data d-flex justify-content-center'>
        <ImageUser classes={'mr-3 image-account-post'} user={user} showLink />
      </div>
    </div>
  );
}

export default Post;
