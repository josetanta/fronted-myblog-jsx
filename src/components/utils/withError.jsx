import { Component } from 'react';
import { Jumbotron } from 'react-bootstrap';

function withError(WrappedComponent) {
  return class extends Component {
    state = {
      hasError: false,
      messageError: null,
    };

    componentDidCatch(error, errorInfo) {
      this.setState({
        hasError: true,
        messageError: error.message + ' ' + errorInfo,
      });
    }

    render() {
      if (this.state.hasError) {
        return (
          <Jumbotron fluid>
            <h1>Los sentimos mucho, pero hubo un error</h1>
            <p>En unos instantes se podrá solucionar el error</p>
          </Jumbotron>
        );
      }
      return <WrappedComponent {...this.props} />;
    }
  };
}

export default withError;
