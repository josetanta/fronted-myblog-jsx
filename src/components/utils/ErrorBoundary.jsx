import { Component } from 'react';
import { Jumbotron, Button } from 'react-bootstrap';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.handleReloadWindow = this.handleReloadWindow.bind(this);
    this.state = {
      hasError: false,
      messageError: '',
    };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      hasError: true,
      messageError: `${error.message} ${errorInfo}`,
    });
  }

  componentWillUnmount() {
    this.setState({
      hasError: false,
    });
  }

  handleReloadWindow() {
    window.location.reload();
  }

  render() {
    const { hasError, messageError } = this.state;

    if (hasError) {
      return (
        <Jumbotron fluid className='p-4'>
          <h1 className='text-warning'>Los sentimos mucho, pero hubo un error</h1>
          <p>En unos instantes se podrá solucionar el error</p>
          <Button onClick={this.handleReloadWindow} variant='outline-success'>
            Cargar la Página
          </Button>
          <br />
          <small className='text-danger'>Error {messageError}</small>
        </Jumbotron>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
