import React from 'react';
import { useDispatch } from 'react-redux';
import { getUserAction } from 'src/app/actions';
import { useUserAuth } from 'src/hooks/authentication';

/**
 * @typedef {{
 *  children: React.ReactNode
 * }} Props
 * @param {Props} props
 * @returns
 */
function AppRoot(props) {
  const dispatch = useDispatch();
  const userAuth = useUserAuth();
  React.useEffect(() => {
    dispatch(getUserAction());
  }, [dispatch]);

  if (!userAuth.isReady)
    return <h4 className='text-center text-uppercase fw-bolder'>Loading....</h4>;
  return <React.Fragment>{props.children}</React.Fragment>;
}

export default AppRoot;
