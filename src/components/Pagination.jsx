import { Col, Pagination as PB } from 'react-bootstrap';

import { PAGES } from 'src/utils/cookies';

function Pagination(props) {
  let items = [];
  for (let n = 1; n <= PAGES; n++) {
    items.push(
      <PB.Item as='span' key={n} onClick={() => props.setPage(n)}>
        Página {n}
      </PB.Item>
    );
  }

  return (
    <Col sm={12} md={12} xl={12}>
      <PB>{items}</PB>
    </Col>
  );
}
export default Pagination;
