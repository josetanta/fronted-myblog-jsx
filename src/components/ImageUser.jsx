import { Link } from 'react-router-dom';

function ImageUser({ user, classes, showLink }) {
  return (
    <div className='d-flex align-items-center'>
      {user && <img className={classes} src={user.image} alt={user.slug} />}
      {(showLink && (
        <Link className='text-decoration-none' to={`/user/${user.uuid}`}>
          <b>{user.username}</b>
        </Link>
      )) ||
        null}
    </div>
  );
}
export default ImageUser;
