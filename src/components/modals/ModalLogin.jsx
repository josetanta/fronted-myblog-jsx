import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { Alert, Form, Modal, Spinner } from 'react-bootstrap';

import { userLoginAction } from 'src/app/actions/userActions';
import { useUserAuth } from 'src/hooks/authentication';
import { parseArrAndObj } from 'src/utils';
import { resetAction } from 'src/app/actions';

const initValues = {
  email: '',
  password: '',
};

function ModalLogin(props) {
  const dispatch = useDispatch();
  const auth = useUserAuth();

  const methodsForm = useForm({ defaultValues: initValues });

  const handleResetActionClose = () => {
    dispatch(resetAction());
  };

  const handleLoginSubmit = (data) => {
    const fd = new FormData();
    fd.append('email', data.email);
    fd.append('password', data.password);
    dispatch(userLoginAction(fd));
    if (methodsForm.errors) props.setModalLogin(true);
    else props.setModalLogin(false);
  };

  return (
    <Modal show={props.modalLogin} onHide={() => props.setModalLogin(!props.modalLogin)}>
      <Modal.Body>
        <div className='d-block register-vi'>
          <legend className='border-bottom mb'>Iniciar Sesión</legend>
          {auth.loading && <Spinner animation='border' variant='primary' />}
          <Alert variant='danger' show={!!auth.error} onClose={handleResetActionClose} dismissible>
            {parseArrAndObj(auth.error) instanceof Array && (
              <React.Fragment>
                {parseArrAndObj(auth.error).map((err, index) => (
                  <span key={'err-auth' + (index + 1)} className='text-danger my-2'>
                    {err}
                  </span>
                ))}
              </React.Fragment>
            )}
          </Alert>

          <Alert
            variant={auth.success ? 'success' : 'danger'}
            show={!!auth.message}
            onClose={handleResetActionClose}
            dismissible
          >
            <span className={auth.success ? 'text-success' : 'text-danger'}>{auth.message}</span>
          </Alert>
          <Form onSubmit={methodsForm.handleSubmit(handleLoginSubmit)}>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                name='email'
                type='email'
                placeholder='example@mail.org'
                ref={methodsForm.register({
                  required: true,
                })}
              />
              {methodsForm.errors.email && <span className='text-danger'>Corrija su Email</span>}
            </Form.Group>
            <Form.Group>
              <Form.Label>Contraseña</Form.Label>
              <Form.Control
                autoComplete='off'
                type='password'
                placeholder='Contraseña'
                name='password'
                ref={methodsForm.register({
                  required: true,
                })}
              />
              {methodsForm.errors.password && (
                <span className='text-danger'>Corrija su Contraseña</span>
              )}
            </Form.Group>
            <Form.Group>
              <input value='Iniciar Sesión' type='submit' className='btn btn-block btn-primary' />
            </Form.Group>
            <div className='d-flex justify-content-between justify-items-center'>
              <Link
                className='text-decoration-none'
                to={'/register'}
                onClick={() => props.setModalLogin(false)}
              >
                <i className='fas fa-sign-in-alt' /> Registrarse
              </Link>
            </div>
          </Form>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default ModalLogin;
