import { Button, Modal } from 'react-bootstrap';

export default function ModalEmail(props) {
  return (
    <Modal
      onHide={() => props.setModalShowEmail(!props.modalShowEmail)}
      show={props.modalShowEmail}
      size='lg'
      aria-labelledby='contained-modal-title-vcenter'
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id='contained-modal-title-vcenter'>Enviar Correo</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Enviar un mensaje al staff</h4>
        <p>
          Hola que tal, puedes enviarnos algunas consultas que usted desea o si tien algun problema
          con sitio web.
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => props.setModalShowEmail(!props.modalShowEmail)}>Cerrar</Button>
      </Modal.Footer>
    </Modal>
  );
}
