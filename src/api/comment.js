import { clientAnonymousRequest, clientTokenRequest } from './axiosClient';

async function getCommentList(postSlug) {
  const { client } = clientAnonymousRequest();
  const { data } = (await client.get(`/posts/${postSlug}/comments/`)).data;
  return {
    data,
  };
}

async function saveComment(comment, postId) {
  try {
    let { client } = clientTokenRequest();
    await client.post(`/comments/`, comment);
    const { data } = await getCommentList(postId);
    return { data };
  } catch (error) {
    return { errors: error.response.data?.errors };
  }
}

async function updateComment(comment, commentId, postId) {
  let { client } = clientTokenRequest();

  await client.put(`/comments/${commentId}/`, comment);

  const { data } = await getCommentList(postId);

  return { data };
}

async function deleteComment(commentId, postId) {
  let { client } = clientTokenRequest();

  await client.delete(`/comments/${commentId}/`);
  const { data } = await getCommentList(postId);
  return {
    data,
  };
}

export { saveComment, getCommentList, updateComment, deleteComment };
