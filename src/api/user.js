import { clientAnonymousRequest, clientTokenRequest } from './axiosClient';

async function getUserDetails(uuid) {
  const { client } = clientAnonymousRequest();
  const { data: user } = (await client.get(`/users/${uuid}/`)).data;
  const { data: posts } = (await client.get(`/users/${uuid}/posts/`)).data;

  return {
    user,
    posts,
  };
}

async function userLogin(userData) {
  const { client } = clientAnonymousRequest();
  const { token, message } = (await client.post('/login/', userData)).data;

  return {
    token,
    message,
  };
}

async function userLogout() {
  const { client } = clientTokenRequest();

  let { user, message } = (await client.post('/logout/')).data;
  return {
    user,
    message,
  };
}

async function registerUser(user) {
  const { client } = clientAnonymousRequest();
  const { data, message, token } = (await client.post(`/users/`, user)).data;

  return {
    data,
    message,
    token,
  };
}

async function updateUser(data) {
  const { client, user } = clientTokenRequest();
  const res = (await client.put(`/users/${user.id}/`, data)).data;

  return {
    data: res.data,
    message: res.message,
  };
}

async function userConfirmAccount(token) {
  const { client } = clientTokenRequest();

  const { message, data } = (await client.post(`/confirm-account/${token}/`)).data;

  return { message, data };
}

async function reseendTokenConfirm() {
  const { client, user } = clientTokenRequest();

  const { message } = (await client.post(`/resend-token/${user.id}/`)).data;

  return { message };
}

async function getUserAccount(userId) {
  const { client } = clientTokenRequest();

  const response = (await client.get(`/users/${userId}/account/`)).data;

  return response.data;
}

async function getUser() {
  const { client } = clientTokenRequest();
  const res = await client.get('/user-auth/');
  return res.data.user;
}

async function removeImageUser() {
  const { client } = clientTokenRequest();
  const res = await client.delete('/remove-image/');
  return { message: res.data.message, user: res.data.user };
}

export {
  getUserAccount,
  getUserDetails,
  registerUser,
  updateUser,
  userLogin,
  userConfirmAccount,
  userLogout,
  reseendTokenConfirm,
  getUser,
  removeImageUser,
};
