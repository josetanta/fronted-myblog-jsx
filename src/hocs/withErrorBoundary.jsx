import ErrorBoundary from 'src/components/utils/ErrorBoundary';

function withErrorBoundary(WrapperComponent) {
  return function Wrapper(props = {}) {
    return (
      <ErrorBoundary>
        <WrapperComponent {...props} />
      </ErrorBoundary>
    );
  };
}

export default withErrorBoundary;
